Are you seeking exceptional NDIS participant services in the Newcastle region including Central Coast, Gosford, Lake Macquarie and Maitland? 
Look no further than what’s offered by NDIS trusted provider, Elleisha’s Property Services. Cleaning, Lawn Mowing, Home Maintenance and Home Modifications.

Address: 118 Marks Point Road, Marks Point, NSW 2280, Australia

Phone: +61 2 4947 7791

Website: https://www.elleishaspropertyservices.com.au/Newcastle
